
# Android App Monitor
Compares various techniques to monitor which app the user currently has open

### Results

| Technique                | Reliability | Min API Level | Max API Level | Permissions           | Delivery |                      Quirks                      | Sees dialog-style apps | Detects activities | Restrictions                                                                    |
|--------------------------|-------------|---------------|---------------|-----------------------|----------|:------------------------------------------------:|------------------------|--------------------|---------------------------------------------------------------------------------|
| getRunningTasks()        | ?           | 1             | 20            | Get running tasks     | Polling  |                                                  | Y | Y                  |                                                                                 |
| getRunningAppProcesses() | ?           | 21            | 21            | None                  | Polling  |                                                  | Y | N                  |                                                                                 |
| AccessibilityService     | Good        | 4             | None          | Accessibility service | Callback | Occasionally sees activity switches out-of-order | Sometimes | Y                  | Google cracked down on these but then reconsidered                              |
| queryEvents()            | ?           | 21            | None          | View app usage        | Polling  |                                                  | Y | Y                  | Docs say most recent data will be truncated, but this is not currently the case |
| queryUsageStats()        | ? | 21            | None          | View app usage        | Polling  | | Y | N                  |                                                                                 |
| /proc                    | Poor        | ?             | 23            | None                  | Polling  | Can't narrow down to a single app                |                        | N                  |                                                                                 |
