package samuelpalmer.appmonitor;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.media.AudioAttributes;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import java.util.List;

import samuelpalmer.appmonitor.detectors.Detector;

public class AppMonitorService extends Service {

    private static final String NOTIFICATION_CHANNEL_MAIN = "main";
    private static final String TAG = AppMonitorService.class.getSimpleName();

    private List<Detector> detectors;

    @SuppressLint("ShowToast")
    @Override
    public void onCreate() {
        super.onCreate();

        Intent mainActivityIntent = new Intent(this, MainActivity.class);
        PendingIntent mainActivityPendingIntent = PendingIntent.getActivity(this, 1, mainActivityIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Notification.Builder notificationBuilder = new Notification.Builder(this)
                .setContentTitle("App monitor running")
                .setContentIntent(mainActivityPendingIntent)
                .setSmallIcon(android.R.drawable.ic_media_ff);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(NOTIFICATION_CHANNEL_MAIN, "Main", NotificationManager.IMPORTANCE_DEFAULT);
            channel.setSound(null, new AudioAttributes.Builder().build());

            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
            notificationBuilder.setChannelId(NOTIFICATION_CHANNEL_MAIN);
        }

        Notification notification = notificationBuilder.getNotification();

        startForeground(1, notification);

        Toast.makeText(this, "See the logs for results", Toast.LENGTH_LONG).show();

        detectors = Detector.makeAllDetectors(this, listener);
        for (Detector detector : detectors)
            detector.start();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        for (Detector detector : detectors)
            detector.stop();

        detectors = null;

        stopForeground(true);
    }

    private final Detector.Listener listener = new Detector.Listener() {
        @Override
        public void onChanged() {
            Log.i(TAG, "Activity changed");
            for (Detector detector : detectors) {
                ComponentName activity = detector.getTopActivity();
                Log.i(TAG, detector.getClass().getSimpleName() + ": " + (activity == null ? null : activity.flattenToShortString()));
            }
        }
    };

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

}
