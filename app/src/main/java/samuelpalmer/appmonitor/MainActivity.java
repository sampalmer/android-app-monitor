package samuelpalmer.appmonitor;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import samuelpalmer.appmonitor.detectors.Detector;

public class MainActivity extends Activity {

    private static final int REQUEST_CODE_ACQUIRE_PERMISSION_OFFSET = 1000;

    /*
    Keeps track of which detectors we still need to ask the user for permission for.
    Some entries will be null, which indicates that permission has been requested.
     */
    private final List<Detector> detectorsPendingPermissions = new ArrayList<>();
    private Intent serviceIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        serviceIntent = new Intent(MainActivity.this, AppMonitorService.class);

        Button startButton = findViewById(R.id.button_start);
        Button stopButton = findViewById(R.id.button_stop);

        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                populateDetectorsPendingPermissions();

                if (!detectorsPendingPermissions.isEmpty())
                    acquireNextPermission();
                else
                    startService();
            }
        });

        stopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopService(serviceIntent);
            }
        });
    }

    private void populateDetectorsPendingPermissions() {
        Detector.Listener dummyListener = new Detector.Listener() {@Override public void onChanged() {}};
        List<Detector> detectors = Detector.makeAllDetectors(MainActivity.this, dummyListener);

        for (Detector detector : detectors)
            if (!detector.hasPermission())
                detectorsPendingPermissions.add(detector);
    }

    private void startService() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(serviceIntent);
        }
        else {
            startService(serviceIntent);
        }
    }

    private void acquireNextPermission() {
        for (int i = 0; i < detectorsPendingPermissions.size(); ++i) {
            Detector detectorPendingPermission = detectorsPendingPermissions.get(i);
            if (detectorPendingPermission != null) {
                startActivityForResult(detectorPendingPermission.makeAcquirePermissionIntent(), REQUEST_CODE_ACQUIRE_PERMISSION_OFFSET + i);
                Toast.makeText(this, detectorPendingPermission.makeAcquirePermissionMessage(), Toast.LENGTH_LONG).show();
                return;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // This code will probably break if this activity died before we got here, but it'll do for the time being

        if (requestCode >= REQUEST_CODE_ACQUIRE_PERMISSION_OFFSET) {
            int detectorIndex = requestCode - REQUEST_CODE_ACQUIRE_PERMISSION_OFFSET;
            detectorsPendingPermissions.set(detectorIndex, null);

            boolean hasPendingPermissions = false;
            for (int i = 0; i < detectorsPendingPermissions.size(); ++i) {
                Detector detectorPendingPermissions = detectorsPendingPermissions.get(i);

                if (detectorPendingPermissions != null) {
                    if (detectorPendingPermissions.hasPermission())
                        detectorsPendingPermissions.set(i, null);
                    else {
                        acquireNextPermission();
                        hasPendingPermissions = true;
                        break;
                    }
                }
            }

            if (!hasPendingPermissions) {
                detectorsPendingPermissions.clear();
                populateDetectorsPendingPermissions();
                if (detectorsPendingPermissions.isEmpty())
                    startService();
                else {
                    detectorsPendingPermissions.clear();
                    Toast.makeText(this, "Not all permissions were granted", Toast.LENGTH_LONG).show();
                }
            }
        }
    }
}
