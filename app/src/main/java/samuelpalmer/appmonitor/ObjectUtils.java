package samuelpalmer.appmonitor;

public abstract class ObjectUtils {
    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public static boolean equals(Object a, Object b) {
        return (a == b) || (a != null && a.equals(b));
    }
}
