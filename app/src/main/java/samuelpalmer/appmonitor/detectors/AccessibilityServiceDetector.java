package samuelpalmer.appmonitor.detectors;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.provider.Settings;

import samuelpalmer.appmonitor.AccessibilityUtils;
import samuelpalmer.appmonitor.AppMonitorAccessibilityService;
import samuelpalmer.appmonitor.R;

public final class AccessibilityServiceDetector extends Detector {

    public AccessibilityServiceDetector(Context context, Listener listener) {
        super(context, listener);
    }

    @Override
    public boolean hasPermission() {
        return AccessibilityUtils.isAccessibilityServiceEnabled(context, AppMonitorAccessibilityService.class);
    }

    @Override
    public Intent makeAcquirePermissionIntent() {
        return new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
    }

    @Override
    public String makeAcquirePermissionMessage() {
        return "Please turn on the accessibility service for " + context.getString(R.string.app_name);
    }

    @Override
    public void start() {
        AppMonitorAccessibilityService.listeners.add(accessibilityServiceSubscription);
        accessibilityServiceSubscription.onActivityChanged();
    }

    @Override
    public void stop() {
        AppMonitorAccessibilityService.listeners.remove(accessibilityServiceSubscription);
    }

    private final AppMonitorAccessibilityService.Listener accessibilityServiceSubscription = new AppMonitorAccessibilityService.Listener() {
        @Override
        public void onActivityChanged() {
            ComponentName topActivity = AppMonitorAccessibilityService.getTopActivity();
            setTopActivity(topActivity);
        }
    };

}
