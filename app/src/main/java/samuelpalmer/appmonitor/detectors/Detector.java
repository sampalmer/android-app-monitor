package samuelpalmer.appmonitor.detectors;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;

import java.util.ArrayList;
import java.util.List;

import samuelpalmer.appmonitor.ObjectUtils;

public abstract class Detector {

    protected final Context context;
    private final Listener listener;

    private ComponentName topActivity;

    public Detector(Context context, Listener listener) {
        if (context == null)
            throw new IllegalArgumentException();
        if (listener == null)
            throw new IllegalArgumentException();

        this.context = context;
        this.listener = listener;
    }

    public abstract boolean hasPermission();
    public abstract Intent makeAcquirePermissionIntent();
    public abstract String makeAcquirePermissionMessage();
    public abstract void start();
    public abstract void stop();
    public ComponentName getTopActivity() { return topActivity; }

    protected void setTopActivity(ComponentName newTopActivity) {
        if (!ObjectUtils.equals(topActivity, newTopActivity)) {
            topActivity = newTopActivity;
            listener.onChanged();
        }
    }

    public interface Listener {
        void onChanged();
    }

    public static List<Detector> makeAllDetectors(Context context, Listener listener) {
        List<Detector> results = new ArrayList<>();

        results.add(new AccessibilityServiceDetector(context, listener));

        if (RunningProcessesPollingDetector.isSupported())
            results.add(new RunningProcessesPollingDetector(context, listener));

        if (RunningTasksPollingDetector.isSupported())
            results.add(new RunningTasksPollingDetector(context, listener));

        if (UsageStatsAggregatePollingDetector.isSupported(context))
            results.add(new UsageStatsAggregatePollingDetector(context, listener));

        if (UsageStatsEventsPollingDetector.isSupported(context))
            results.add(new UsageStatsEventsPollingDetector(context, listener));

        if (NativeRunningProcessesPollingDetector.isSupported())
            results.add(new NativeRunningProcessesPollingDetector(context, listener));

        return results;
    }

}
