package samuelpalmer.appmonitor.detectors;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import com.jaredrummler.android.processes.AndroidProcesses;
import com.jaredrummler.android.processes.models.AndroidAppProcess;

import java.util.List;

/**
 * Uses this third-party library to read the list of running processes from the OS:
 * https://github.com/jaredrummler/AndroidProcesses
 */
public class NativeRunningProcessesPollingDetector extends PollingDetector {

    public NativeRunningProcessesPollingDetector(Context context, Listener listener) {
        super(context, listener);
    }

    @Override
    protected ComponentName pollInternal() {
        List<AndroidAppProcess> runningForegroundApps = AndroidProcesses.getRunningForegroundApps(context);
        String packageName = runningForegroundApps.get(0).getPackageName();
        return new ComponentName(packageName, "");
    }

    public static boolean isSupported() {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.N;
    }

    @Override
    public boolean hasPermission() {
        return true;
    }

    @Override
    public Intent makeAcquirePermissionIntent() {
        return null;
    }

    @Override
    public String makeAcquirePermissionMessage() {
        return null;
    }

}
