package samuelpalmer.appmonitor.detectors;

import android.content.ComponentName;
import android.content.Context;
import android.os.Handler;

abstract class PollingDetector extends Detector {

    private static final int POLL_INTERVAL_MILLIS = 500;

    private final Handler handler;

    public PollingDetector(Context context, Listener listener) {
        super(context, listener);
        this.handler = new Handler();
    }

    @Override
    public void start() {
        poll.run();
    }

    @Override
    public void stop() {
        handler.removeCallbacks(poll);
    }

    private final Runnable poll = new Runnable() {
        @Override
        public void run() {
            ComponentName topActivity = pollInternal();
            setTopActivity(topActivity);
            handler.postDelayed(poll, POLL_INTERVAL_MILLIS);
        }
    };

    protected abstract ComponentName pollInternal();

}
