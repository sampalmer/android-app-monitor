package samuelpalmer.appmonitor.detectors;

import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import java.lang.reflect.Field;
import java.util.List;

@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class RunningProcessesPollingDetector extends PollingDetector {

    /**
     * Process is hosting the current top activities.  Note that this covers all activities that are visible to the user.
     */
    private static final int PROCESS_STATE_TOP = 2;

    private final ActivityManager am;
    private final Field processStateField;

    public RunningProcessesPollingDetector(Context context, Listener listener) {
        super(context, listener);
        am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);

        try {
            //noinspection JavaReflectionMemberAccess
            processStateField = ActivityManager.RunningAppProcessInfo.class.getDeclaredField("processState");
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected ComponentName pollInternal() {
        List<ActivityManager.RunningAppProcessInfo> processes = am.getRunningAppProcesses();
        for (ActivityManager.RunningAppProcessInfo process : processes) {
            if (
                    // Filters out most non-activity processes
                    process.importance <= ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND
                    &&
                    // Filters out processes that are just being _used_ by the process with the activity
                    process.importanceReasonCode == ActivityManager.RunningAppProcessInfo.REASON_UNKNOWN
            ) {
                int state;
                try {
                    state = processStateField.getInt(process);
                } catch (IllegalAccessException e) {
                    throw new RuntimeException(e);
                }

                if (state == PROCESS_STATE_TOP) {
                    String[] processNameParts = process.processName.split(":");
                    String packageName = processNameParts[0];

                    /*
                     If multiple candidate processes can get here,
                     it's most likely that apps are being switched.
                     The first one provided by the OS seems to be
                     the one being switched to, so we stop here.
                     */

                    return new ComponentName(packageName, "");
                }
            }
        }

        return null;
    }

    public static boolean isSupported() {
        return Build.VERSION.SDK_INT == 21;
    }

    @Override
    public boolean hasPermission() {
        return true;
    }

    @Override
    public Intent makeAcquirePermissionIntent() {
        return null;
    }

    @Override
    public String makeAcquirePermissionMessage() {
        return null;
    }

}
