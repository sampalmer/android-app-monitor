package samuelpalmer.appmonitor.detectors;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import java.util.List;

public class RunningTasksPollingDetector extends PollingDetector {

    private final ActivityManager am;

    public RunningTasksPollingDetector(Context context, Listener listener) {
        super(context, listener);
        am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
    }

    @Override
    protected ComponentName pollInternal() {
        List<ActivityManager.RunningTaskInfo> runningTasks = am.getRunningTasks(1);
        if (runningTasks != null && !runningTasks.isEmpty()) {
            ActivityManager.RunningTaskInfo topRunningTask = runningTasks.get(0);
            if (topRunningTask != null)
                return topRunningTask.topActivity;
        }

        return null;
    }

    public static boolean isSupported() {
        return Build.VERSION.SDK_INT < 21;
    }

    @Override
    public boolean hasPermission() {
        return true;
    }

    @Override
    public Intent makeAcquirePermissionIntent() {
        return null;
    }

    @Override
    public String makeAcquirePermissionMessage() {
        return null;
    }

}
