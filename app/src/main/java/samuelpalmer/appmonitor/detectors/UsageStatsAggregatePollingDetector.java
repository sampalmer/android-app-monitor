package samuelpalmer.appmonitor.detectors;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.ComponentName;
import android.content.Context;
import android.os.Build;

import java.util.List;

@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class UsageStatsAggregatePollingDetector extends UsageStatsPollingDetector {

    @SuppressLint("InlinedApi")
    public UsageStatsAggregatePollingDetector(Context context, Listener listener) {
        super(context, listener);
    }

    @Override
    protected ComponentName pollInternal() {
        long now = System.currentTimeMillis();

        // TODO: Only look back as far as your last call to this method to save performance
        // TODO: Investigate whether performance can be improved by using a larger interval (eg yearly)
        List<UsageStats> apps = usageStatsManager.queryUsageStats(UsageStatsManager.INTERVAL_DAILY, now - 1000 * 1000, now);

        long latestEventTime = -1;
        String latestEventPackage = null;

        if (apps != null)
            for (UsageStats app : apps)
                // Need to filter out the "android" package since it comes up when the screen is locked even though it isn't really an app or activity
                if (app.getPackageName() != null && !app.getPackageName().equals("android") && app.getLastTimeUsed() > latestEventTime) {
                    latestEventPackage = app.getPackageName();
                    latestEventTime = app.getLastTimeUsed();
                }

        if (latestEventPackage == null)
            return null;
        else
            return new ComponentName(latestEventPackage, "");
    }

}
