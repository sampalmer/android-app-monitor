package samuelpalmer.appmonitor.detectors;

import android.annotation.TargetApi;
import android.app.usage.UsageEvents;
import android.content.ComponentName;
import android.content.Context;
import android.os.Build;
import android.util.Log;

@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class UsageStatsEventsPollingDetector extends UsageStatsPollingDetector {

    private long lastActivityTime;

    public UsageStatsEventsPollingDetector(Context context, Listener listener) {
        super(context, listener);
    }

    @Override
    public void start() {
        super.start();
        lastActivityTime = 0;
    }

    @Override
    protected ComponentName pollInternal() {
        long nowMillis = System.currentTimeMillis();

        // Only looking back as far as the last known activity to minimise resource usage
        UsageEvents usageEvents = usageStatsManager.queryEvents(lastActivityTime > 0 ? lastActivityTime - 1 : 0, nowMillis);
        boolean gotEvents = false;
        long latestEventTime = -1;
        String latestEventPackage = null;
        String latestEventClassName = null;

        if (usageEvents != null) {
            UsageEvents.Event event = new UsageEvents.Event();

            while (usageEvents.hasNextEvent()) {
                gotEvents = true;

                // Apparently you need to always loop through all available events to avoid a crash: https://stackoverflow.com/a/50896301/238753
                usageEvents.getNextEvent(event);

                if (event.getEventType() == UsageEvents.Event.MOVE_TO_FOREGROUND && event.getTimeStamp() > latestEventTime) {
                    // It looks safe to assume that MOVE_TO_FOREGROUND is for an activity: https://raw.githubusercontent.com/aosp-mirror/platform_frameworks_base/master/services/core/java/com/android/server/am/ActivityManagerService.java
                    latestEventPackage = event.getPackageName();
                    latestEventClassName = event.getClassName();
                    latestEventTime = event.getTimeStamp();
                }
            }
        }

        // TODO: Do something properly here
        if (!gotEvents) {
            // Apparently some devices report that permission is granted when it's actually not, resulting in this situation.
            // See https://stackoverflow.com/a/43583005/238753.
            Log.w(getClass().getSimpleName(), "Didn't get any usage events. Usage permission is probably missing.");
            throw new RuntimeException("Didn't get any usage events. Usage permission is probably missing.");
        }

        if (latestEventTime > lastActivityTime)
            lastActivityTime = latestEventTime;

        if (latestEventPackage == null) {
            Log.w(getClass().getSimpleName(), "Didn't find the top activity. Re-using last known activity.");

            // We didn't get an update, so it's probably safe to assume that the last known activity is still current
            return getTopActivity();
        }
        else
            return new ComponentName(latestEventPackage, latestEventClassName == null ? "" : latestEventClassName);
    }

}
