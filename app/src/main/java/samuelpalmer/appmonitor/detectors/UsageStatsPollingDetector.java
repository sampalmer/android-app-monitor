package samuelpalmer.appmonitor.detectors;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AppOpsManager;
import android.app.usage.UsageStatsManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Process;
import android.provider.Settings;

import samuelpalmer.appmonitor.R;

@TargetApi(Build.VERSION_CODES.LOLLIPOP)
abstract class UsageStatsPollingDetector extends PollingDetector {

    protected final UsageStatsManager usageStatsManager;

    @SuppressLint("InlinedApi")
    public UsageStatsPollingDetector(Context context, Listener listener) {
        super(context, listener);
        usageStatsManager = (UsageStatsManager) context.getSystemService(Context.USAGE_STATS_SERVICE);
    }

    public static boolean isSupported(Context context) {
        PackageManager packageManager = context.getPackageManager();
        Intent intent = new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS);
        return Build.VERSION.SDK_INT >= 21 && packageManager.resolveActivity(intent, 0) != null;
    }

    @Override
    public boolean hasPermission() {
        // This permission check is taken from https://medium.com/@quiro91/show-app-usage-with-usagestatsmanager-d47294537dab
        AppOpsManager appOps = (AppOpsManager) context.getSystemService(Context.APP_OPS_SERVICE);
        int mode = appOps.checkOpNoThrow(AppOpsManager.OPSTR_GET_USAGE_STATS, Process.myUid(), context.getPackageName());
        return mode == AppOpsManager.MODE_ALLOWED;
    }

    @Override
    public Intent makeAcquirePermissionIntent() {
        return new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS);
    }

    @Override
    public String makeAcquirePermissionMessage() {
        return "Please grant usage access to " + context.getString(R.string.app_name);
    }

}
